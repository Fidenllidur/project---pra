#include <Arduino.h>
#include <FreeRTOS.h>
#include <NewPing.h>

#define MAX_DISTANCE 512
#define TRIGGER_PIN 23
#define ECHO_PIN 22
#define SONAR_NUM 2

NewPing sonar[SONAR_NUM] = {
	NewPing(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE),
	NewPing(21, 19, MAX_DISTANCE)};
int distances[SONAR_NUM] = {0, 0};

void sensorsTaskC0(void *parameters);
void communicationTaskC1(void *parameters);
void controllTaskC1(void *parameters);

void getSonicSensors();
void infraRedSensor(int inputPin);
int filterInput(int inputValue);

void setup()
{
	xTaskCreatePinnedToCore(
		sensorsTaskC0,
		"Task on core 0",
		1024,
		NULL,
		1,
		NULL,
		0);
	xTaskCreatePinnedToCore(
		communicationTaskC1,
		"Communication task on core 1",
		1024,
		NULL,
		1,
		NULL,
		1);
	xTaskCreatePinnedToCore(
		controllTaskC1,
		"Controll task on core 1",
		1024,
		NULL,
		1,
		NULL,
		1);
	Serial.begin(921600);
}

void loop()
{
	//vTaskDelay(1000);
}

void sensorsTaskC0(void *parameters)
{
	for (;;)
	{
		getSonicSensors();
		//Serial.println("AAAAAAAAAAAAAAAAAAAA");
		yield();
	}
}

void communicationTaskC1(void *parameters)
{

	for (;;)
	{
		for (int i = 0; i < SONAR_NUM; i++)
		{
			Serial.print("Ping");
			Serial.print(i);
			Serial.print(": ");
			Serial.print(distances[i]);
			Serial.println(" cm");
		}
		// Serial.println(distances[0]);
		yield();
	}
}
void controllTaskC1(void *parameters)
{
	pinMode(2, OUTPUT);
	for (;;)
	{
		if (distances[0] < 15 || distances[1] < 15)
		{
			digitalWrite(2, LOW);
		}
		else
		{
			digitalWrite(2, HIGH);
		}
	}
}

void getSonicSensors()
{
	for (int i = 0; i < SONAR_NUM; i++)
	{
		distances[i] = sonar[i].ping_cm();
	}
}

void infraRedSensor(int inputPin)
{
	//TODO
}

int output = 0;
int filterInput(int inputValue)
{
	vTaskDelay(100);
	output = inputValue;
	return output;
}