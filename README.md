# **Projekt Praxe - cz**

### **Autonomní automobil**
Pomocí modelu auta s různými senzory a vývojové desky esp32 udělám autonomní auto. K programování esp32 použiju jazyk c++.  
Model auta je schopný zatáčet, a jezdit různou rychlostí. Auto je poháněné dvěma motory a akumulátorem. Přední kola zatáčí pomocí servo motoru.

**Model by měl být schopný**: 
* Být ovládaný z mobilní aplikace
* Detekovat překážky
* Měřit vlastní rychlost pomocí akcelerometru
* Parkovat
* Odbočovat na křižovatkách
* Jezdit v pruhu
* Předjíždět překážky na cestě

## **Řešení**
### **Senzory**
* Ultrasonický senzor vzdálenosti
* Infračervený senzor vzdálenosti
* Akcelerometr
### **Další možné senzory**
Nejsem si jistý, že je budu schopný použít, ale bylo by skvělé mít data z těhle senzorů.
* eps32-cam
    * OpenCV ??
    * Pro detekci čar na cestě
    * Případně pro zjištění druhu překážky
        * záleží na zařízení, které by data spracovávalo

### **Procesory/desky**
* esp32 dev board v1
![alt text](ESP32-Pinout.jpg "ESP32-Pinout.jpg")
* Arduino

### **Knihovny**
* FreeRTOS
* NewPing
* ??OpenCV??

<!--```cpp
void Update() {
    println("Karel");
}
```-->
# **School project - en**

### **Autonomous car**
I'll create a model of car with various sensors and esp32 dev kit v1.

**The model should be able to: **: 
* be controlled from a mobile phone
* detect obstacles
* measure it's speed with the use of a accelerometer
* park
* turn on junctions
* drive in lane
* overtake obstacles on road

## **How it's done**
### **Parts**
**Sensors**
* Ultrasonic distance sensors
* Infrared distance sensors
* Accelerometer
**Other possible sensors**
I'm not sure that I'll be able to use these but it would be nice to have the data
* eps32-cam
    * OpenCV ??
    * For lane detection
    * Possibly even for better identifying obstacles
        * that depend on what will be used to process the data

**Processors/Dev boards**
* esp32 dev board v1
![alt text](ESP32-Pinout.jpg "ESP32-Pinout.jpg")
* Arduino

**Libraries**
* FreeRTOS
* NewPing
* ??OpenCV??